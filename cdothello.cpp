#include "cdothello.h"
#include <vector>

// Board weights and evaluation function are based pretty heavily on
// http://mkorman.org/othello.pdf
int board_weights[8][8] = {{20, -3, 11, 8, 8, 11, -3, 20}, \
                          {-3, -7, -4, 1, 1, -4, -7, -3}, \
                          {11, -4, 2, 2, 2, 2, -4, 11}, \
                          {8, 1, 2, -3, -3, 2, 1, 8}, \
                          {8, 1, 2, -3, -3, 2, 1, 8}, \
                          {11, -4, 2, 2, 2, 2, -4, 11}, \
                          {-3, -7, -4, 1, 1, -4, -7, -3}, \
                          {20, -3, 11, 8, 8, 11, -3, 20}, \
                          };
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    /* Keep track of our side, if necessary? */
    this->side = side;
    this->opp_side = oppSide(side);
    /* Initialize the board so we can keep track of it. */
    board = new Board();
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    delete board;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
/*
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    board->doMove(opponentsMove, opp_side);
    if (board->hasMoves(side)) {
        // If we have moves at all...
        int best_score;
        Move *best_move = new Move(-1, -1);
        //int depth = board->taken.count() / 20;
        //best_score = alphaBeta2(board, side, 2, (int) -INFINITY, (int) INFINITY, best_move);
        minimax2(board, side, 5, best_move, best_score);
        board->doMove(best_move, side);
        return best_move;
    }
    return NULL;
}

/* Returns the opposite of side s. */
Side ExamplePlayer::oppSide(Side s) {
        return (s == BLACK) ? WHITE : BLACK;
}

/* Fills vec with all valid moves for side on board. */
void ExamplePlayer::validMoves(Board* board, Side side, vector<Move*> &vec) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *move = new Move(i, j);
            if (board->checkMove(move, side)) {
                vec.push_back(move);
            }
        }
    }
}

/* 
 * Alpha-beta prune our minimax search. Negamax flavor.
 */
int ExamplePlayer::alphaBeta(Board* board, Side side, int depth, int alpha, int beta, Move *move) {
    if (depth == 0) {
        return scoreBoard(board);
    }
    else {
        std::vector<Move*> allMoves;
        validMoves(board, side, allMoves); // Fill move vector
        if (allMoves.size() == 0) {
            // At a terminal node.
            return scoreBoard(board);
        }
        std::vector<Move*>::iterator moveIter;
        for (moveIter = allMoves.begin(); moveIter != allMoves.end(); ++moveIter) {
            Move *temp_move = new Move((*moveIter)->getX(), (*moveIter)->getY());
            Board *temp_board = board->copy();
            temp_board->doMove(temp_move, side);
            int val = -alphaBeta(temp_board, oppSide(side), depth - 1, -beta, -alpha, move);
            if (val >= beta) {
                move = temp_move;
                return val;
            }
            else if (val >= alpha) {
                alpha = val;
            }
        }
        return alpha;
    }
}

/* 
 * Alpha: our best.
 * Beta: opponent's best.
 */
int ExamplePlayer::alphaBeta2(Board* board, Side side, int depth, int alpha, int beta, Move *chosen_move) {
    if (depth == 0) {
        int s = scoreBoard(board);
        //std::cerr << "returning score: " << s << "\n";
        return s;
    }
    // Generate the children
    std::vector<Move*> allMoves;
    validMoves(board, side, allMoves);
    // No moves. We're a terminal node.
    if (allMoves.size() == 0) { return scoreBoard(board); }
    // Set up the move iterator.
    std::vector<Move*>::iterator i = allMoves.begin();
    if (side == this->side) {
        // If it's the maximizing player
        while (i != allMoves.end() && alpha < beta) {
            // End conditions
            //std::cerr << "we're maximizing\n";
            //std::cerr << "Trying this move: " << (*i)->getX() << ", " << (*i)->getY() << " on level " << depth << "\n";
            Board *child = board->copy();
            Move *temp_move = new Move((*i)->getX(), (*i)->getY());
            child->doMove((*i), side);
            int val = alphaBeta2(child, oppSide(side), depth - 1, alpha, beta, temp_move);
            chosen_move = temp_move;
            delete child;
            //delete temp_move;
            if (val > alpha) { 
                //std::cerr << "changed alpha from " << alpha << " to " << val << "\n";
                //std::cerr << "this is the maximum score we are assured of so far " << val << "\n";
                alpha = val;
            }
            ++i;
        }
        //chosen_move->setX((*(i-1))->getX());
        //chosen_move->setY((*(i-1))->getY());
        //chosen_move->setX((*(--i))->getX());
        //chosen_move->setY((*(--i))->getY());
        std::cerr << "returning alpha = " << alpha << "\n";
        return alpha;
    }
    else {
        // Minimizing player.
        while (i != allMoves.end() && alpha < beta) {
            //std::cerr << "we're minimizing\n";
            //std::cerr << "Trying this move: " << (*i)->getX() << ", " << (*i)->getY() << " on level " << depth << "\n";
            Board *child = board->copy();
            Move *temp_move = new Move((*i)->getX(), (*i)->getY());
            chosen_move = temp_move;
            child->doMove((*i), side);
            int val = alphaBeta2(child, oppSide(side), depth - 1, alpha, beta, temp_move);
            delete child;
            //delete temp_move;
            if (val < beta) { 
                //std::cerr << "changed beta from " << beta << " to " << val << "\n";
                //std::cerr << "this is the minimum score opponent is assured of so far " << val << "\n";
                beta = val;
            }
            ++i;
        }
        //chosen_move->setX((*(i-1))->getX());
        //chosen_move->setY((*(i-1))->getY());
        chosen_move->setX((*(--i))->getX());
        chosen_move->setY((*(--i))->getY());
        std::cerr << "returning beta = " << beta << "\n";
        return beta;
    }
}


/*
 * Working minimax.
 */
void ExamplePlayer::minimax2(Board* board, Side side, int depth, Move *chosen_move, int &chosen_score) {
    if (depth == 0) {
        chosen_score = scoreBoard(board);
    }
    else {
        std::vector<Move*> allMoves;
        validMoves(board, side, allMoves);
        if (allMoves.size() == 0) {
            // side has no moves. Terminal node.
            chosen_score = scoreBoard(board);
        }
        else {
            // side has some moves! evaluate them.
            // Our side tries to maximize, opponent side tries to minimize.
            int best_score = (side == this->side) ? (int) -INFINITY : (int) INFINITY;
            Move* best_move = new Move(-1, -1);
            std::vector<Move*>::iterator moveIter;
            for (moveIter = allMoves.begin(); moveIter != allMoves.end(); ++moveIter) {
                // Loop through all the available moves and fill in scores.
                Board *new_board = board->copy();
                Move *temp_move = new Move((*moveIter)->getX(), (*moveIter)->getY());
                new_board->doMove(temp_move, side);
                int temp_score;
                minimax2(new_board, oppSide(side), depth - 1, temp_move, temp_score);
                if (side == this->side) {
                    // If temp is better than best - we try to maximize
                    if (temp_score > best_score) {
                        best_score = temp_score;
                        best_move->setX((*moveIter)->getX());
                        best_move->setY((*moveIter)->getY());
                    }
                }
                else {
                    // opponent tries to minimize
                    if (temp_score < best_score) {
                        best_score = temp_score;
                        best_move->setX((*moveIter)->getX());
                        best_move->setY((*moveIter)->getY());
                    }
                }
                delete temp_move;
                delete new_board; // Free memory.
            } // end for
            chosen_move->setX(best_move->getX());
            chosen_move->setY(best_move->getY());
            delete best_move;
            chosen_score = best_score;
        }
    }
}

/* 
 * Score the current board. A higher score indicates a better board for
 * this->side, while a lower score is a better score for this->opp_side. We base
 * our evaluation algorithm heavily on http://mkorman.org/othello.pdf. We take
 * into account piece difference, corner occupancy, corner closeness, mobility,
 * frontier discs, and disc squares.
 * legal moves count and frontier discs as our main evaluation components.  Disc
 * count is basically irrelevant until the end, and corner squares are very
 * important.
 * 
 * @param board: pointer to the board to be scored
 * @return: score of the board for Side side. higher is better.
 */
int ExamplePlayer::scoreBoard(Board *board) { 
    float score = 0;
    float piece_diff_score = pieceDiffScore(board);
    float corner_occ_score = cornerOccScore(board);
    float corner_close_score = cornerCloseScore(board);
    float mobility_score = mobilityScore(board);
    float frontier_score = frontierScore(board);
    float disc_sq_score = discSqScore(board);
    score = 10.0 * piece_diff_score + 801.724 * corner_occ_score + 382.026 * corner_close_score + 78.922 * mobility_score + 74.396 * frontier_score + 10.0 * disc_sq_score;
    return (int) score;
}

/* Compute the piece difference score of board. We simply count the number of
 * our pieces and the number of opponent pieces.
 *
 * @param[in] board: current board.
 * @return the piece difference score of board.
 *
 */
float ExamplePlayer::pieceDiffScore(Board *board) {
    int num_ours = board->count(this->side);
    int num_opp = board->count(this->opp_side);
    if (num_ours > num_opp) {
        // We have more pieces yay
        return (float) 100.0 * num_ours / (num_ours + num_opp);
    }
    else if (num_ours < num_opp) {
        // Opponent has more pieces noo =[
        return (float) -100.0 * num_opp / (num_ours + num_opp);
    }
    else {
        // Same number of pieces
        return 0.0;
    }
}

/* 
 * Compute the mobility score of board. Mobility is very important to have - a
 * very bad situation in Othello occurs when one player doesn't have any moves
 * to make and is forced to pass the turn. We calculate mobility by finding the
 * number of possible moves we and our opponent can make.
 *
 * @param[in] board: current board.
 * @return the mobility score of board.
 *
 */
float ExamplePlayer::mobilityScore(Board *board) {
    int num_ours = numLegalMoves(board, this->side);
    int num_opp = numLegalMoves(board, this->opp_side);
    //std::cerr << "num_legal_sum: " << num_ours + num_opp << "\n";
    if (num_ours + num_opp == 0) {
        // No one has moves, let's not divide by 0.
        return 0.0;
    }
    if (num_ours > num_opp) {
        // We have more yay
        return (float) 100.0 * num_ours / (num_ours + num_opp);
    }
    else if (num_ours < num_opp) {
        // Opponent has more noo =[
        return (float) -100.0 * num_opp / (num_ours + num_opp);
    }
    else {
        // We have an equal number of legal moves
        return 0.0;
    }
}

/*
 * Compte the corner occupancy score of board. Corners are very important to
 * occupy, as they can never be captured. We count the number of our pieces in
 * corners and the number of opponent pieces in corners to compute this score.
 *
 * @param[in] board: current board.
 * @return the corner occupancy score of board.
 *
 */
float ExamplePlayer::cornerOccScore(Board *board) {
    return (float) 25.0 * numCornerSquares(board, this->side) - 25.0 * numCornerSquares(board, this->opp_side);
}

/* 
 * Compute the corner closeness score of board. Squares adjacent to corners can
 * are generally bad to occupy. We compute this score by counting the number of
 * our pieces adjacent to empty corners and the number of opponent pieces
 * adjacent to empty corners.
 
 * @param[in] board: current board.
 * @return the corner closeness score of board.
 *
 */
float ExamplePlayer::cornerCloseScore(Board *board) {
    return (float) -12.5 * numAdjCornerSquares(board, this->side) + 12.5 * numAdjCornerSquares(board, this->opp_side);
}
/* 
 * Compute the frontier disc score of board, relative to our side.
 *
 * @param[in] board: The current board.
 * @return the frontier disc score of board.
 *
 */
float ExamplePlayer::frontierScore(Board *board) {
    int num_opp = numFrontierDiscs(board, this->opp_side);
    int num_ours = numFrontierDiscs(board, this->side);
    //std::cerr << "num_frontiers_sum: " << num_ours + num_opp << "\n";
    if (num_ours + num_opp == 0) {
        // No one has frontier discs, let's not divide by 0.
        return 0.0;
    }
    if (num_ours > num_opp) {
        // We have more frontier discs =[
        return (float) -100.0 * (num_ours / (num_ours + num_opp));
    }
    else {
        // Opponent has more frontier discs =]
        return (float) 100.0 * (num_opp / (num_ours + num_opp));
    }
}

/* Calculate the number of frontier discs side has on board. */
int ExamplePlayer::numFrontierDiscs(Board *board, Side side) {
    int num_frontier_discs = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (board->get(side, i, j)) {
                // Ignore corners.
                if (isCorner(i, j)) {
                    break;
                }
                if (i == 0 || i == 7) {
                    // First and last rows - only left and right matter
                    if (!(board->occupied(i, j - 1) && board->occupied(i, j + 1))) {
                            num_frontier_discs++;
                    }
                }
                else if (j == 0 || j == 7) {
                    // First and last columns - only up and down matter
                    if (!(board->occupied(i - 1, j) && board->occupied(i + 1, j))) {
                        num_frontier_discs++;
                    }
                }
                else {
                    // We aren't on the outer square
                    if (!(board->occupied(i - 1, j) && board->occupied(i - 1, j - 1) && board->occupied(i, j - 1) && board->occupied(i + 1, j - 1) && board->occupied(i + 1, j) && board->occupied(i + 1, j + 1) && board->occupied(i, j + 1) && board->occupied(i - 1, j + 1))) {
                        // Check all 8 directions for occupation - if a
                        // a single one is not occupied, we are adjacent to
                        // an empty square.
                        num_frontier_discs++;
                    }// This checks all 8 directions.. maybe just check 4*/
                }
            }
        }
    }
    return num_frontier_discs;
}

/*
 * Compute the "disc square score" of the board, relative to our side. This
 * uses a score for each square on the board. If we occupy that square, we
 * gain the value for that square. If the opponent occupies it, he gains
 * that value.
 *
 * @param board: the current board.
 * @return the disc square score of board.
 *
 */
float ExamplePlayer::discSqScore(Board *board) {
    float score = 0.0;
    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            if (board->get(this->side, i, j)) {
                score += board_weights[i][j];
            }
            else if (board->get(this->opp_side, i, j)) {
                score -= board_weights[i][j];
            }
        }
    }
    return score;
}

/* Compute number of corner squares side has on board. */
int ExamplePlayer::numCornerSquares(Board *board, Side side) {
    int num_corner_squares = 0;
    if (board->get(side, 0, 0)) {
        num_corner_squares++;
    }
    if (board->get(side, 0, 7)) {
        num_corner_squares++;
    }
    if (board->get(side, 7, 0)) {
        num_corner_squares++;
    }
    if (board->get(side, 7, 7)) {
        num_corner_squares++;
    }
    return num_corner_squares;
}

/* Returns the number of pieces next to empty corners side has on board. */
int ExamplePlayer::numAdjCornerSquares(Board *board, Side side) {
    int n = 0;
    if (!(board->occupied(0, 0))) {
        // Top left is empty
        if (board->get(side, 0, 1)) n++;
        if (board->get(side, 1, 0)) n++;
        if (board->get(side, 1, 1)) n++;
    }
    if (!(board->occupied(0, 7))) {
        // Bottom left is empty
        if (board->get(side, 0, 6)) n++;
        if (board->get(side, 1, 7)) n++;
        if (board->get(side, 1, 6)) n++;
    }
    if (!(board->occupied(7, 0))) {
        // Top right is empty
        if (board->get(side, 6, 0)) n++;
        if (board->get(side, 7, 1)) n++;
        if (board->get(side, 6, 1)) n++;
    }
    if (!(board->occupied(7, 7))) {
        // Bottom right is empty
        if (board->get(side, 7, 6)) n++;
        if (board->get(side, 6, 7)) n++;
        if (board->get(side, 6, 6)) n++;
    }
    return n;
}

/* Returns the number of legal moves for side on board. */
int ExamplePlayer::numLegalMoves(Board *board, Side side) {
    int num_legal_moves = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *move = new Move(i, j);
            if (board->checkMove(move, side)) {
                num_legal_moves++;
            }
            delete move;
        }
    }
    return num_legal_moves;
}

int ExamplePlayer::scoreMove(Move *move) {
    int x = move->getX();
    int y = move->getY();
    if (ExamplePlayer::isCorner(x, y)) {
        return CORNER_SCORE;
    }
    if (ExamplePlayer::isSide(x, y)) {
        if (ExamplePlayer::isCSide(x, y)) {
            // We want to check if it is a "good" C square
            // It is good if the edge it is on has 5 or more disks
            if (x == 0 || x == 7) {
                // We are on left or right edge
                int num_edge = 0;
                for (int i = 0; i < 8; i++) {
                    if (board->occupied(x, i)) {
                        num_edge++;
                    }
                }
                if (num_edge >= 5) {
                    return SIDE_SCORE;
                }
            }
            if (y == 0 || y == 7) {
                // We are on left or right edge
                int num_edge = 0;
                for (int i = 0; i < 8; i++) {
                    if (board->occupied(i, y)) {
                        num_edge++;
                    }
                }
                if (num_edge >= 5) {
                    return SIDE_SCORE;
                }
            }
            return C_SCORE;
        }
        return SIDE_SCORE;
    }
    if (ExamplePlayer::isXSquare(x, y)) {
        return X_SCORE;
    }
    if (ExamplePlayer::isInnerSquare(x, y)) {
        return INNER_SQUARE_SCORE;
    }
    return NORMAL_SCORE;
}

bool ExamplePlayer::isInnerSquare(int x, int y) {
    // Is our move on the second layer square (from the outside?)
    return x == 1 || x == 6 || y == 1 || y == 6;
}

bool ExamplePlayer::isCorner(int x, int y) {
    return (x == 0 || x == 7) && (y == 0 || y == 7);
}

/*
 * Checks if move is on the side.
 */
bool ExamplePlayer::isSide(int x, int y) {
    return x == 0 || x == 7 || y == 0 || y == 7;
}

/* 
 * Check if move is on the X-square and side. We already know that move is
 * on the side if this function is called.
 * 
 */
bool ExamplePlayer::isCSide(int x, int y) {
    return x == 1 || x == 6 || y == 1 || y == 6;
}

/* Check if move is in the X-square but not on side */
bool ExamplePlayer::isXSquare(int x, int y) {
    if (x == 1 || x == 6) {
        if (y == 1 || y == 6) {
            return true;
        }
    }
    return false;
}
