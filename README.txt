Othello player. Currently checks depth-1 minimax with board evaluation
heuristics.

Working heuristics include mobility maximization, frontier disc
minimization, and corner-favoring.

Questions:


Describe how and what each group member contributed for the past two weeks. If you are solo, these points will be part of the next section.

In these past two weeks, Cody and Dan have read both the lecture slides and many online websites about heuristics, the minimax algorithm, and alpha-beta pruning.  We took a crash course in othello and played quite a few games against different online AIs.  For contributions, we mainly worked together on the project.  However, in specific:


Dan started out writing the heuristic function, which played moves based solely upon differently valued positions on the board.  Work was started on the minimax function.  After the first one turned out to be flawed, a second minimax function written that did not depend on the side of the player, but just made a raw evaluation of the board using this-> side.  This second minimax function turned out to be the one that was actually wrong, and the first minimax function was actually only a few lines away from being correct.  Afterwards, the concepts of alpha-beta pruning were researched and picturized, and these questions were answered.

Cody then continued the heuristic function and revamped it, changing it to be based less on position, and more on disc count, frontier squares, and mobility.  Work was continued on the scoreBoard function, with the rest of the criteria mentioned below added in.  The minimax function was also fixed, critically by creating a pointer to a temp_move and then passing it in and changing it inside the function so that the move with the best score was passed back out, and the whole thing was combined into a working player that could beat BetterPlayer, ConstantTimePlayer, and SimplePlayer.  Then attempts were made at adding alpha-beta pruning to the minimax algorithm.  As of now, work is still ongoing.




Document the improvements that your group made to your AI to make it tournament-worthy. Explain why you think your strategy(s) will work. Feel free to discuss any ideas were tried but didn't work out


Our AI first began as an extremely simple heuristic program, that is to say, we assigned values to different spots on the board, with priority being: corners > sides > normal squares > side + c-square > inner square, where c-square denotes the squares adjacent to a corner, and inner-square denotes the squares that are adjacent to a side.  Although this function fared decently against Simple Player, it was soon clear that it could not even win consistently, i.e. 8 wins of 10.  As such, we changed our heuristic function to now loop through all valid moves, and for each, create a copy of the board on which we make that move, and then score the resulting board using a heuristic function that assigned values to different positions on the board.  Once again, this was not consistent enough, so more heuristics were added onto this scoreBoard function.  These new heuristics considered the mobility (number of moves that can be made) and tried to minimize the opponenent's mobility while maximizing its own, frontier squares (number of its own markers touching an empty square), and again gave extra importance to corner squares.  

The next week, we started to improve our heuristics (score-board function) and then implemented minimax, that would go down to a given depth.  The scoreboard function was based largely off of a paper written in 2003.  The new function contained piece difference, taking corners, taking spots next to corners, mobility, frontier discs, positional values.  By Monday 5pm, our function was able to beat Better player as black at a depth of 3 and 5, and as white at a depth of 3 and 5, however, for depth 4, it would lose.  However, by Monday 11pm, we tinkered with the heuristic scoreBoard function, and we were able to beat Betterplayer as black at depths of 3,4 and 5, and as white at depths of 4 and 5.  

As of 11:30pm, we are working on adding alpha-beta pruning to our minimax algorithm, as we want to have our code go to depths of more than 5-6 without getting absurdly slow.  We were also thinking of maybe recoding the board class and denoting by a string of chars or by several integers in order to make the code run faster.  We were also thinking of adding an opening book to make the calculation and speed a lot easier in the opening stages of the games.  Lastly, we are planning to try to make our player brute-force all possible remaining moves if we are around 10 moves away from game over (54 discs on the board).

This strategy should work because in essence, our player is looking 4-5 moves ahead at every move, and determining what will be the end result of every possibility.  It also assumes that the opponent will be trying to make us lose, and thus tries gain the most from each opponent's move.  Also, as it looks several moves into the future, it will be less likely to fall afoul of traps or accidentally giving up corners.  Our heuristics are also very important in this case.  We look above all to capture the corners, but mainly try to limit the amount of moves the opponent can make, as well as minimize the number of empty squares adjacent to our own squares, in essence, protecting our flanks.  We believe that this strategy will allow us to do relatively well in the tournament.
