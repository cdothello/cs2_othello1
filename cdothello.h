#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <vector>

#define CORNER_SCORE  6
#define SIDE_SCORE  5
#define NORMAL_SCORE  4
#define C_SCORE  3
#define INNER_SQUARE_SCORE 2
#define X_SCORE  1

using namespace std;

class ExamplePlayer {

private:
    Board *board;
    Side side;
    Side opp_side;

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    Side oppSide(Side s);
    void validMoves(Board* board, Side side, std::vector<Move*> &vec);
    int alphaBeta2(Board* board, Side side, int depth, int alpha, int beta, Move *chosen_move);
    int alphaBeta(Board* board, Side side, int depth, int alpha, int beta, Move *move);
    void minimax(Board* board, int depth, int max, Move* move, int* score);
    void minimax2(Board* board, Side side, int depth, Move *bestest_move, int &bestest_score);
    int scoreMove(Move *move);
    int scoreBoard(Board *board);
    float cornerOccScore(Board *board);
    float mobilityScore(Board *board);
    float cornerCloseScore(Board *board);
    float pieceDiffScore(Board *board);
    float frontierScore(Board *board);
    float discSqScore(Board *board);
    int numCornerSquares(Board *board, Side side);
    int numAdjCornerSquares(Board *board, Side side);
    int numLegalMoves(Board *board, Side side);
    int numFrontierDiscs(Board *board, Side side);
    bool isInnerSquare(int x, int y);
    bool isCorner(int x, int y);
    bool isSide(int x, int y);
    bool isCSide(int x, int y);
    bool isXSquare(int x, int y);
};

#endif
